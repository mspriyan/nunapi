from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class NunUser(models.Model):
  user = models.OneToOneField(User, on_delete=models.CASCADE)
  bio = models.TextField(max_length=500, blank=True)
  location = models.CharField(max_length=30, blank=True)
  birth_date = models.DateField(null=True, blank=True)

  @receiver(post_save, sender=User)
  def create_nunuser(sender, instance, created, **kwargs):
    if created and not instance.is_superuser:
      NunUser.objects.create(user=instance)

  @receiver(post_save, sender=User)
  def save_nunuser(sender, instance, **kwargs):
    if not instance.is_superuser:
      instance.nunuser.save()

class Post(models.Model):

  title = models.CharField(max_length=120, blank=True, null=True)
  content = models.CharField(max_length=400, null=True)
  posted_by = models.ForeignKey('NunUser', related_name='user_posts', on_delete=models.CASCADE)
  image_uri = models.FileField(upload_to="images/", blank=True)
  created_date = models.DateTimeField(auto_now=True)

  def __str__(self):
    return self.title
  
  class Meta:
    ordering = ["-created_date"]

class Comment(models.Model):

  id = models.AutoField(primary_key=True)
  post = models.ForeignKey('Post', related_name='comments', on_delete=models.CASCADE)
  commented_on = models.ForeignKey('Post', on_delete=models.CASCADE, null=True)
  comment = models.CharField(max_length=200, blank=True, null=True)
  created_date = models.DateTimeField(auto_now=True)

  def __str__(self):
    return self.id

  class Meta:
    ordering = ["-created_date"]