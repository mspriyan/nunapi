import graphene

class UserInput(graphene.InputObjectType):
  username = graphene.String()
  password = graphene.String()

class PostInput(graphene.InputObjectType):
  title = graphene.String()
  content = graphene.String()