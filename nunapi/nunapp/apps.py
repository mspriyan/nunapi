from django.apps import AppConfig


class NunappConfig(AppConfig):
    name = 'nunapp'
