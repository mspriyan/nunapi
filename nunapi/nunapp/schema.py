import graphene
from graphene_django.types import DjangoObjectType
from graphene_file_upload.scalars import Upload
from graphql_jwt.decorators import login_required 
from .models import User,Post,Comment, NunUser
from .inputs import UserInput, PostInput

class UserType(DjangoObjectType):
  class Meta:
    model = User


class PostType(DjangoObjectType):
  class Meta:
    model = Post


class CommentType(DjangoObjectType):
  class Meta:
    model = Comment

class NunUserType(DjangoObjectType):
  class Meta:
    model = NunUser


class Query(object):
  all_users = graphene.List(UserType)
  all_posts = graphene.List(PostType)
  me = graphene.List(UserType)

  def resolve_all_users(self, info, **kwargs):
    return User.objects.all()

  def resolve_all_posts(self, info, **kwargs):
    return Post.objects.all()

  @login_required
  def resolve_me(self, info, **kwargs):
    return User.objects.get(pk=info.context.user.id)


class CreateUser(graphene.Mutation):
  class Arguments:
    input = UserInput(required=True)

  ok = graphene.Boolean()
  user = graphene.Field(UserType)

  def mutate(root, info, input=None):
    ok = True
    user = User.objects.create_user(input.username, password=input.password)
    return CreateUser(ok=ok, user=user)


class CreatePost(graphene.Mutation):
  class Arguments:
    input = PostInput(required=False)
    file = Upload(required=False)

  ok = graphene.Boolean()
  post = graphene.Field(PostType)

  @login_required
  def mutate(root, info, file=None, input=None):
    ok = True
    post = Post(title=input.title, content=input.content, posted_by=info.context.user.nunuser)
    if file:
      post.image_uri = file
    post.save()
    return CreatePost(ok=ok, post=post)

class CreateComments(graphene.Mutation):
  class Arguments:
    id = graphene.ID(required=True)
    text = graphene.String()

  ok = graphene.Boolean()
  comment = graphene.Field(CommentType)

  @login_required
  def mutate(root, info, **kwargs):
    print(kwargs)
    ok = True
    for_post = Post.objects.get(pk=kwargs["id"])
    comment = Comment(post=for_post, comment=kwargs["text"])
    comment.save()
    return CreateComments(ok=ok, comment=comment)


class Mutation(object):
  create_user = CreateUser.Field()
  create_post = CreatePost.Field()
  create_comment = CreateComments.Field()